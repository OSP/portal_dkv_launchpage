class RegistrationsController < Devise::RegistrationsController
    respond_to :json

    # referensi Request Register
    #
    # { 
    #    user[email]=bambangso@gmail.com
    #    user[password]=12345678
    #    user[user_biography_attributes][name]=bambang parkoso
    # }

    def create
        temp_user = User.generate_username(params[:user][:user_biography][:name])
        user = User.new(user_params.merge(username: temp_user))
        if user.save
            user.user_coin_point.coin = 0
            user.user_coin_point.point = 0
            user.user_biography.name = params[:user][:user_biography][:name]
            user.user_biography.avatar = "https://cdn2.iconfinder.com/data/icons/ios-7-icons/50/user_male2-512.png"
            user.save
            email_history = UserMailHistory.create(user_id: user.id, email: user.email) 
            render :json=> user.as_json( :email => user.email ), :status=>201
            return
        else
            warden.custom_failure!
            render :json => user.errors, :status=>422
        end
    end

    private
    def user_params
        params.require(:user).permit(:email,:password,:username, user_coin_point_attributes: [:coin, :poin], user_biography_attributes: [:name, :avatar]).merge(username_changed: false, dewa: false)
    end
end  