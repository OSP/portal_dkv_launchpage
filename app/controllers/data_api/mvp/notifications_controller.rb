class DataApi::Mvp::NotificationsController < ApplicationController
    before_action :authenticate_user!
    respond_to :json

    def index
        notification_list
        render json: @notification_list.where("users.id = " current_user.id) if current_user.present?
    end

    def create
        notification = Notification.new(notification_params)
        if notification.save
            notification.save
        else
            warden.custom_failure!
            render :json => notification.errors, :status=>422
        end
    end

    private
    def notification_list
        @notification_list = Notification.joins(:user, :notification_type).select("notifications.id, notification_types.name_type as jenis_notifikasi, notifications.message as pesan, notifications.url as asal_notifikasi, notifications.thumbnail as gambar, notifications.seen as sudah_dilihat").all
    end
    
    def notification_params
        params.require(:notification).permit(:notification_type_id,:message,:description, :category_id, :user_id).merge(user_id: current_user.id)
    end
end