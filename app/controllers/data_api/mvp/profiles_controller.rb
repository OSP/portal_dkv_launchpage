class DataApi::Mvp::ProfilesController < ApplicationController
    before_action :authenticate_user!
    respond_to :json

    def show
        user_list
        video_list

        case @user_data.jenis_kelamin
        when "1"
            @user_data.jenis_kelamin = "Laki - Laki"
        when "2"
            @user_data.jenis_kelamin = "Perempuan"
        when "0"
            @user_data.jenis_kelamin = "Tidak Ingin Diketahui"
        end

        @my_profiles = [@user_data, @video_list];
        render json: @my_profiles
    end

    def update
        @user = User.find(current_user.id)
        @user_profile = UserBiography.where("user_id = #{@user.id}").first
        @user_profile.update(user_params)
        unless params[:user_biography][:email] == @user.email
            @user.update(email: params[:user_biography][:email])
            @email_history = UserMailHistory.create(user_id: @user.id, email: params[:user_biography][:email])
        end
    end

    private 
    def video_list
        @video_list = Video.joins(:user, :category).select("videos.id, videos.name as nama_video, videos.url as alamat_video, users.username as nama_kreator, categories.name as kategori, videos.description as deskripsi_video, videos.created_at as tanggal_unggah").where("users.id = #{current_user.id}")
    end

    def user_list
        @user_data = UserBiography.select("id, name as nama_pengguna, cast(gender as varchar) as jenis_kelamin, avatar as foto_profil").where("user_id = #{current_user.id}").first
    end

    def user_params
        params.require(:user_biography).permit(:name,:avatar,:gender)
    end
end