class DataApi::Mvp::TempatDewa::ValidatingVideosController < ApplicationController
    before_action :authenticate_user!
    respond_to :json

    def index
        video_list
        render json: @video_list
    end

    def update
        @video_pick = Video.find(params[:id])
        case params[:valid]
        when "valid"
            @video_pick.update(validasi: 3, validation_date: DateTime.now())
        when "unvalid"
            @video_pick.update(validasi: 2, validation_date: DateTime.now())
        end
    end

    private
    def video_list
        @video_list = Video.joins(:user, :category).select("videos.id, videos.name as nama_video, videos.url as alamat_video, videos.user_id as user_id, users.username as nama_kreator, categories.name as kategori, videos.description as deskripsi_video, videos.created_at as tanggal_unggah").all
    end
end