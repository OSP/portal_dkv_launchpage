# Kode Validasi
# 1 = Waiting For Validation
# 2 = Rejected
# 3 = Validated

class DataApi::Mvp::VideosController < ApplicationController
    before_action :authenticate_user!, only: :create
    respond_to :json

    def index
        video_list
        if params[:milari].present?
            render json: @video_list.where("lower(videos.name) like '%" + params[:milari] + "%'")
        else
            render json: @video_list
        end
    end

    def create
        video = Video.new(video_params)
        if video.save
            video_name = video.name rescue ''
            notification_message = 'Video "'+ video.name + '" sedang dalam proses validasi[title-not_title]Kami membutuhkan waktu untuk memastikan video kamu sesuai, sabar ya..'
            notification = Notification.new(user_id: video.user_id, notification_type_id: 2, message: notification_message, seen: false)
            notification.save
        else
            warden.custom_failure!
            render :json => video.errors, :status=>422
        end
    end

    def show
        video_list
        render json: @video_list.find(params[:id])
    end

    private
    def video_list
        @video_list = Video.joins(:user, :category).select("videos.id, videos.name as nama_video, videos.url as alamat_video, users.username as nama_kreator, categories.name as kategori, videos.description as deskripsi_video, videos.created_at as tanggal_unggah").where("videos.validasi = 3")
    end

    def video_params
        params.require(:video).permit(:name,:url,:description, :category_id, :user_id, :validasi).merge(user_id: current_user.id, validasi: 1)
    end
end