class DataApi::Mvp::HistoriesController < ApplicationController
    before_action :authenticate_user!
    respond_to :json

    def index
        history_list
        render json: @history_list.where("users.id = " current_user.id) if current_user.present?
    end

    def create
        history = History.new(history_params)
        if history.save
            history.save
        else
            warden.custom_failure!
            render :json => history.errors, :status=>422
        end
    end

    private
    def history_list
        @history_list = History.joins(:user).select("histories.id, histories.message as pesan, histories.url as asal_notifikasi, histories.thumbnail as gambar, histories.seen as sudah_dilihat").all
    end
    
    def history_params
        params.require(:history).permit(:history_type_id,:message,:description, :category_id, :user_id).merge(user_id: current_user.id)
    end
end