class NotificationType < ApplicationRecord
    has_many :notifications
end