Rails.application.routes.draw do

  devise_for :users,
             path: '',
             path_names: {
               sign_in: 'masuk',
               sign_out: 'keluar',
               registration: 'daftar'
             },
             controllers: {
               sessions: 'sessions',
               registrations: 'registrations'
             }

  namespace :data_api, default: {format: :json} do
    namespace :mvp do
      resources :videos, only: [:index, :show, :search, :create]
      resources :profiles, only: [:show, :update]
      resources :notifications, only: [:index, :create]
      namespace :tempat_dewa do
        resources :validating_videos, only: [:index, :update]
      end
    end
  end

end