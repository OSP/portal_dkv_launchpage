class ReviseNotification < ActiveRecord::Migration[6.0]
  def change
    add_column :notifications, :url, :string
    add_column :notifications, :thumbnail, :string
    add_column :videos, :thumbnail, :string
  end
end
