class CreateNotificationTypes < ActiveRecord::Migration[6.0]
  def change
    create_table :notification_types do |t|
      t.string :name_type
      t.timestamps
    end
    add_foreign_key :notifications, :users
    add_foreign_key :notifications, :notification_types
  end
end
