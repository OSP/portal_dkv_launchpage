class VideoValidation < ActiveRecord::Migration[6.0]
  def change
    add_column :videos, :validate, :integer
    add_column :videos, :validation_date, :datetime
    add_column :videos, :validation_by, :string
    add_column :videos, :validation_message, :string
  end
end