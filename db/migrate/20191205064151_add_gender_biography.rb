class AddGenderBiography < ActiveRecord::Migration[6.0]
  def change
    add_column :user_biographies, :gender, :int, :null => false, :default => 0
  end
end