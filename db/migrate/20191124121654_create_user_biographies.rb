class CreateUserBiographies < ActiveRecord::Migration[6.0]
  def change
    create_table :user_biographies do |t|
      t.integer :user_id
      t.string :name
      t.string :description
      t.string :avatar
      t.timestamps
    end
  end
end
