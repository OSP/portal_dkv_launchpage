class DefaultCoinPoint < ActiveRecord::Migration[6.0]
  def change
    change_column_default :user_coin_points, :coin, to: 0
    change_column_default :user_coin_points, :point, to: 0
  end
end