class CreateUserCoinPoints < ActiveRecord::Migration[6.0]
  def change
    create_table :user_coin_points do |t|
      t.integer :user_id
      t.integer :coin
      t.integer :point
      t.timestamps
    end
  end
end
