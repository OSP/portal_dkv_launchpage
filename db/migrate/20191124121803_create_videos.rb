class CreateVideos < ActiveRecord::Migration[6.0]
  def change
    create_table :videos do |t|
      t.integer :user_id
      t.string :name
      t.string :url
      t.string :description
      t.string :category_id
      t.timestamps
    end
  end
end
