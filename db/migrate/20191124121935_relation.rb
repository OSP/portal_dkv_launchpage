class Relation < ActiveRecord::Migration[6.0]
  def change
    change_column :videos ,:category_id, 'integer USING CAST(category_id AS integer)'
    change_column :articles ,:category_id, 'integer USING CAST(category_id AS integer)'
    add_foreign_key :user_mail_histories, :users
    add_foreign_key :user_coin_points, :users
    add_foreign_key :user_coin_histories, :user_coin_points
    add_foreign_key :user_point_histories, :user_coin_points
    add_foreign_key :user_biographies, :users
    add_foreign_key :quotes, :users
    add_foreign_key :user_interests, :users

    add_foreign_key :user_video_view_histories, :users
    add_foreign_key :user_video_view_histories, :videos
    add_foreign_key :user_article_view_histories, :users
    add_foreign_key :user_article_view_histories, :articles
    add_foreign_key :videos, :users
    add_foreign_key :videos, :categories
    add_foreign_key :articles, :users
    add_foreign_key :articles, :categories
  end
end