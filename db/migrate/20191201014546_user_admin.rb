class UserAdmin < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :username_changed, :boolean
    add_column :users, :dewa, :boolean
  end
end