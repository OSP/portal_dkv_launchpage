class CreateUserVideoViewHistories < ActiveRecord::Migration[6.0]
  def change
    create_table :user_video_view_histories do |t|
      t.integer :user_id
      t.integer :video_id
      t.timestamps
    end
  end
end
