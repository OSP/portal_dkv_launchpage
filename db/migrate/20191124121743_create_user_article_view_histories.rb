class CreateUserArticleViewHistories < ActiveRecord::Migration[6.0]
  def change
    create_table :user_article_view_histories do |t|
      t.integer :user_id
      t.integer :article_id
      t.timestamps
    end
  end
end
