class CreateNotifications < ActiveRecord::Migration[6.0]
  def change
    create_table :notifications do |t|
      t.integer :user_id
      t.integer :notification_type_id
      t.string :reference_id
      t.string :message
      t.boolean :seen
      t.timestamps
    end
  end
end
