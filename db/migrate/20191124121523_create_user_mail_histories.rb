class CreateUserMailHistories < ActiveRecord::Migration[6.0]
  def change
    create_table :user_mail_histories do |t|
      t.integer :user_id
      t.string :email
      t.timestamps
    end
  end
end
