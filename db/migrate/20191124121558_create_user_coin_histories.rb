class CreateUserCoinHistories < ActiveRecord::Migration[6.0]
  def change
    create_table :user_coin_histories do |t|
      t.integer :user_coin_point_id
      t.integer :coin
      t.string :type
      t.timestamps
    end
  end
end
