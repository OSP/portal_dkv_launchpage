# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Default Notification Type
NotificationType.create(name_type: "Coin Purchase")
NotificationType.create(name_type: "Video Validation")
NotificationType.create(name_type: "Subscribed Video Update")
NotificationType.create(name_type: "Video Comment")
NotificationType.create(name_type: "Video Like")
NotificationType.create(name_type: "Promotion")
NotificationType.create(name_type: "Admin Message")

# Default Category
Category.create(name: "Fotografi")
Category.create(name: "Sinematografi")
Category.create(name: "Ilustrasi")
Category.create(name: "Tipografi")
Category.create(name: "Animasi")

# Default User
admin = []
admin[0] = User.create(email: "rinaldes.duma@gmail.com", password: "tetapcogan", username: "R", confirmed_at: DateTime.now, dewa: true, username_changed: false)
admin[0].user_biography.name = "Rinaldes Duma"
admin[1] = User.create(email: "yusril.ibnu@gmail.com", password: "cintakamu", username: "yusril", confirmed_at: DateTime.now, dewa: true, username_changed: false)
admin[1].user_biography.name = "Yusril Ibnu Maulana"
admin[2] = User.create(email: "andikapratama5669@gmail.com", password: "cintakamu", username: "andika", confirmed_at: DateTime.now, dewa: true, username_changed: false)
admin[2].user_biography.name = "Andika Pratama Wijaya"
admin[3] = User.create(email: "alvinakmali@gmail.com", password: "cintakamu", username: "alvin", confirmed_at: DateTime.now, dewa: true, username_changed: false)
admin[3].user_biography.name = "Alvin Akmal Imam"

admin.each do |mimin|
    mimin.user_coin_point.coin = 0
    mimin.user_coin_point.point = 0
    mimin.user_biography.avatar = "https://cdn2.iconfinder.com/data/icons/ios-7-icons/50/user_male2-512.png"
    mimin.user_biography.gender = 1
    mimin.save
    UserMailHistory.create(user_id: mimin.id, email: mimin.email)
end

